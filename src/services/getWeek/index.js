import React, {Component} from 'react';
import {CONCAT_CITY} from '../../constants/apiWeather';

const getWeekInfoByCityName = cityName => {

    const promise = new Promise((resolve, reject) => {
        fetch(CONCAT_CITY('forecast', cityName)).then(res => {
            return res.json();
        }).then(res =>{
            resolve(res);
        }).catch(err => {
            reject(err);
        })
    });

    return promise;

}

export default getWeekInfoByCityName;
