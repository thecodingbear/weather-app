import React, {Component} from 'react';
import './style.css';

class Footer extends Component{
    render() {
        return (
            <div>
                <footer>
                    Información obtenida desde <a href="https://openweathermap.org/">openweathermap.org</a>
                </footer>
            </div>
        )
    }
}

export default Footer;
