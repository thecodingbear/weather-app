import React, {Component} from 'react';
import Header from '../header';
import getCurrentInfoByCityName from '../../services/getCurrentCity';
import Week from '../week';
import Footer from '../footer';
import store from '../../store';
import {getBackgroundByState} from '../../constants/backgrounds';
import Loader from '../loader';
import './style.css';
import {CITY} from '../../constants';

class Background extends Component {
    constructor() {
        super();
    }

    styleBackground = () => {
        return {
            backgroundImage : 'url(https://www.hotelmanagement.com.au/wp-content/uploads/2012/03/GC-Hotels-Mantra-Sun-City-View.jpg)'
        }
    }

    render() {

        return (
            <div>
                <div style={this.styleBackground()} id="Background">
                    <div id="Overlay">
                        <div id="Container">
                            <Header></Header>
                            <Week></Week>
                            <Footer></Footer>
                        </div>
                    </div>
                </div>
            </div>
        );

    }

}

export default Background;
