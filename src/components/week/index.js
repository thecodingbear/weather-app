import React, {Component} from 'react';
import getWeekInfoByCityName from '../../services/getWeek';
import {store} from '../../store';
import {connect} from 'react-redux';
import kelvinToCelsius from 'kelvin-to-celsius';
import moment from 'moment';
import {CITY} from '../../constants/';
import {URL_ICOS} from '../../constants/apiWeather';

import './style.css';

class Week extends Component {
    constructor() {
        super();
        this.state = {
            ready : null,
            week : []
        };
    }

    getArrayFiltered(list){

        var promise = new Promise(function(resolve, reject) {

            var array = {
                0 : null,
                1 : null,
                2 : null,
                3 : null,
                4 : null,
                5 : null,
                6 : null
            }

            var flag = false;

            for (var i = 0; i < list.length; i++) {
                if(array[moment.unix(list[i].dt).day()] === null){
                    array[moment.unix(list[i].dt).day()] = {
                        weekDay : moment.unix(list[i].dt).format('ddd'),
                        min : kelvinToCelsius(list[i].main.temp_min),
                        max : kelvinToCelsius(list[i].main.temp_max),
                        icon : list[i].weather[0].icon,
                        description : list[i].weather[0].description
                    }
                }

                if(i + 1 === list.length){
                    resolve(array);
                }

            }

        });

        return promise;

    }

    componentDidMount() {
        getWeekInfoByCityName(CITY).then(res => {
            return this.getArrayFiltered(res.list);
        }).then(res => {

            this.setState({
                ready : true,
                week : [...this.state.week,
                    res[1],
                    res[2],
                    res[3],
                    res[4],
                    res[5],
                    res[6],
                    res[0]
                ]
            })
        })
    }

    render() {
        if(this.state.ready){
            return (
                <div id="WeekContent">
                    {this.state.week.map((x, k) => {
                        if(x != null){
                            return (
                                <div key={k} onClick={() => this.props.updateCurrentWeather(x)}>
                                    <h4>{x.weekDay}</h4>
                                    <img src={URL_ICOS + x.icon + '.png'} />
                                    <div className="maxMin">
                                        <span>{parseInt(x.min)} &ordm;</span>
                                        <span>/</span>
                                        <span>{parseInt(x.max)} &ordm;</span>
                                    </div>
                                    <div className="description" title={x.description}>
                                        {x.description}
                                    </div>
                                </div>
                            )
                        }
                    })}
                </div>
            );
        }else{
            return 'Cargando...';
        }

    }

}

const mapStateToStore = store =>{
    return {

    }
}

const mapDispatchToProps = dispatch => {
    return {
        updateCurrentWeather(selected){
            dispatch({
                type : "UPDATE_CURRENT_WEATHER",
                forecast : {
                    weekDay : selected.weekDay,
                    min : selected.min,
                    max : selected.max,
                    description : selected.description
                }
            })
        }
    }
}

export default connect(mapStateToStore, mapDispatchToProps)(Week);
