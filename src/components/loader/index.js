import React, {Component} from 'react';

import './style.css';

class Loader extends Component{
    constructor() {
        super();
    }

    render() {
        return (
            <div>
                <div id="Loader">
                    <p>Cargando...</p>
                </div>
            </div>
        );
    }

}

export default Loader;
