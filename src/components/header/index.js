import React, {Component} from 'react';
import kelvinToCelsius from 'kelvin-to-celsius';
import Background from '../background';
import {store} from '../../store';
import {connect} from 'react-redux';
import Loader from '../loader';
import {URL_ICOS} from '../../constants/apiWeather';
import {CITY, DAYS} from '../../constants/';
import getCurrentInfoByCityName from '../../services/getCurrentCity';

import './style.css';

class Header extends Component {
    constructor() {
        super();
        this.state = {
            ready : null,
            currentData : {
                cityName : null,
                icon : null,
                temperature : null,
                description : null
            },
            forecast : null
        }

    }

    componentDidMount() {
        getCurrentInfoByCityName(CITY).then(res => {
            this.setState({
                ready : true,
                currentData : {
                    cityName : res.name,
                    icon : res.weather[0].icon,
                    temperature : Math.round(kelvinToCelsius(res.main.temp)),
                    description : res.weather[0].description
                }
            })
        })
    }

    render() {
        if(this.state.ready){
            if(this.props.forecast.weekDay == undefined){
                return (
                    <header>
                        <h2>{this.state.currentData.cityName}</h2>
                        <img src={URL_ICOS + this.state.currentData.icon + '.png'} />
                        <p className="grades">Temperatura actual<br /><span>{this.state.currentData.temperature} &ordm;</span></p>
                        <p className="description">{this.state.currentData.description}</p>
                        <hr />
                    </header>
                );
            }else{
                return (
                    <header>
                        <h2>{this.state.currentData.cityName}</h2>
                        <img src={URL_ICOS + this.state.currentData.icon + '.png'} />
                        <p className="grades forecast">
                            <span>{DAYS(this.props.forecast.weekDay)}</span>
                            <span className="min"><i>Min</i> {parseInt(this.props.forecast.min)} &ordm;</span>
                            <span>/</span>
                            <span className="max"><i>Max</i> {parseInt(this.props.forecast.max)} &ordm;</span>
                        </p>
                        <p className="description">{this.props.forecast.description}</p>
                        <hr />
                    </header>
                );
            }
        }else{
            return <Loader></Loader>;
        }

    }

}

const mapStateToStore = state => {
    return {
        forecast : state.forecast
    }
}

const mapDispatchToProps = dispatch => {
    return {};
}

export default connect(mapStateToStore, mapDispatchToProps)(Header);
