export const CITY = 'Montevideo,uy';

export const DAYS = day => {

    let days = {
        Mon : 'Lunes',
        Tue : 'Martes',
        Wed : 'Miércoles',
        Thu : 'Jueves',
        Fri : 'Viernes',
        Sat : 'Sábado',
        Sun : 'Domingo'
    }

    return days[day];

}
