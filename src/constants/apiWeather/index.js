const API_KEY = '&appid=3c09e07fb566c06ae7c4117f13367b19';

const URL_API = 'https://api.openweathermap.org/data/2.5/';

// type = data type requested
// city = city need data
export const CONCAT_CITY = (type, city) => {
    return URL_API + type + '?q=' + city + API_KEY + '&lang=es';
}

export const URL_ICOS = 'http://openweathermap.org/img/w/';
