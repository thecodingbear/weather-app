// sol
// nublado
// lluvioso
const SOL = 'https://www.hotelmanagement.com.au/wp-content/uploads/2012/03/GC-Hotels-Mantra-Sun-City-View.jpg';
const NUBLADO = 'https://cdn.pixabay.com/photo/2015/11/07/11/43/city-1031435_960_720.jpg';
const LLUVIOSO = 'https://images.musement.com/default/0001/29/rainydayamsterdam-jpg_header-28651.jpeg?&q=60&fit=crop&lossless=true&auto=format&w=944&h=350';

export const getBackgroundByState = state => {

    const promise = new Promise(function(resolve, reject) {

        switch (state) {
            case 'Clear':
                    resolve(SOL);
                break;
            case 'Few':

                break;
            case 'scattered':

                break;
            case 'broken':

                break;
            case 'shower':

                break;
            case 'Rain':
                    resolve(LLUVIOSO);
                break;
            case 'thunderstorm':

                break;
            case 'snow':

                break;
            case 'Mist':
                    resolve(NUBLADO);
                break;
            default:
        }

    });

    return promise;

}
