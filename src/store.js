import { createStore } from 'redux';

const defaultStore = {
    forecast : {
        cityName : null,
        icon : null,
        temperature : null,
        description : null
    }
}

const reducer = (state, action) => {
    switch (action.type) {
        case "UPDATE_CURRENT_WEATHER":

            return {
                ...state,
                forecast : action.forecast
            }

        break;
        default:

    }

    return state;
}

export const store = createStore(reducer, defaultStore);
