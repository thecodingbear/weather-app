import React, { Component } from 'react';
import Background from './components/background';

import './index.css';

class App extends Component {
  render() {
    return (
        <div className="App">
            <Background></Background>
        </div>
    );
  }
}

export default App;
